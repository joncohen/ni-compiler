module N1Passes where
import Env
import Data.List
import N1

-------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Pass: Uniquify Variable names

-- for our case, a result will simply be an Either String a
type Result a = Either String a

-- we define a return type, or result type, for our compiler
-- because we need to return information about a particular pass
-- along with the result of the pass, which is often a transformation
-- of the AST. You can think of this in some ways as an accumulator
-- for a fold operation over an AST.
data CompilerResult a b = CState a (Result b) 
	deriving (Show, Eq)


-- Pass: Uniquify N1
-- this is the state we'll use for the uniquify pass, it has
-- an environment which we'll adjust as we enter let expressions
-- and an Integer which will be used for generating symbol names
data UniquifyState = UState (Env String) Integer 
	deriving (Show, Eq)

-- a uniquify result is a compiler result with our state and an N1 Exp
type UniquifyResult = CompilerResult UniquifyState Exp

-- a helper function to pull out the result from the compiler result
getResult :: CompilerResult a N1 -> Result N1
getResult (CState _ (Right p)) = Right p
getResult (CState _ err) = err


{--
  The uniquify pass transforms an N1 program into another
  N1 program, but ensures that every variable name is unique!
--}
uniquify :: N1 -> CompilerResult UniquifyState N1
uniquify (Program exp) =
  case uniquifyExp exp (UState Env.makeEnv 0) of
    CState state (Right exp') -> CState state $ Right $ Program exp'
    CState state (Left msg) -> CState state $ Left msg

{--
  uniquifyExp transforms an expression in N1 to another
  expression in N1 where the variables have been renamed. We
  pass UniquifyState along with the Exp and get a result. Note
  that this result also contains a UniquifyState, which allows
  us to update it from calls down further in the AST as we 
  recurse through it. 
--}
uniquifyExp :: Exp -> UniquifyState -> UniquifyResult

-- uniquifying an Int is simple, it doesn't change the state,
-- and just returns the int expression
uniquifyExp v@(Int _) state = CState state $ Right v

-- uniquifying Read is also straightforward, the state doesn't change
uniquifyExp r@Read state = CState state $ Right r

-- uniquifying a Negate requires uniquifying its subexpression, which
-- then requires 
uniquifyExp (Negate exp) state =
	case uniquifyExp exp state of
		CState state' (Right res) -> CState state' (Right (Negate res) )
		err -> err

-- You must fill out the following:
uniquifyExp (Add x y) state = 
	case uniquifyExp x state of 
		CState state' (Right res1) -> case uniquifyExp y state' of 
			CState state'' (Right res2) -> CState state'' (Right (Add res1 res2) )
			err -> err
		err -> err

uniquifyExp (Var sym) state@(UState env cnt) = 
	case Env.lookup sym env of
		Right v -> CState state (Right (Var v) )
		Left err -> CState state (Left err)
		
uniquifyExp (Let sym exp body) state@(UState env cnt) = 
	case uniquifyExp exp state of 
		CState state' (Right res) -> 
			let newName = makeSymName cnt in
			let newEnv = extendEnv sym newName env in
			let newState = UState newEnv (cnt + 1) in 
				case uniquifyExp body newState of
					CState state'' (Right bodyRes) -> CState state'' (Right (Let newName res bodyRes) ) 
					err -> err
		err -> err
	
-------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Pass: Remove Complex Operations 

-- we will have slightly different types this time around, but still very similar:
type RCOState = Integer
type RCOResult = CompilerResult RCOState Exp

-- we're changing our pass slightly to simply take a CompilerResult as an argument
-- and return a CompilerResult
passRemoveComplexOperas :: CompilerResult RCOState N1 -> CompilerResult RCOState N1

-- straightforward since we just have one kind of program that contains an expression
passRemoveComplexOperas (CState symCount (Right (Program expr))) =
    case rcoExp (CState symCount (Right expr)) of
      CState symCount (Right exp') -> CState symCount $ Right $ Program exp'
      CState symCount (Left msg) -> CState symCount $ Left msg

-- rcoExp walks through expressions that can be complex or atomic
rcoExp :: RCOResult -> RCOResult
rcoExp atm@(CState _ (Right (Int _))) = atm		-- 114
rcoExp atm@(CState _ (Right (Var _))) = atm
rcoExp atm@(CState _ (Right (Read))) = atm		-- 116
		
-- negate expressions, which need atomic subexpressions
rcoExp (CState state (Right (Negate expr))) =
	case rcoAtm (CState state (Right (expr, []))) of
		CState cnt' (Right (expr', xs)) -> 
			CState cnt' (Right (foldl' foldExps (Negate (expr')) xs))
		CState cnt' (Left msg) -> 
			CState 0 (Left msg)

-- add expressions, subexpressions must be atomic
rcoExp (CState state (Right (Add a b)))  =
	case rcoAtm (CState state (Right (a, []) )) of
		CState cnt' (Right (bodya, binds)) -> 
			case rcoAtm (CState cnt' (Right (b, binds))) of
				CState cnt'' (Right (bodyb, binds')) -> 
						-- Add should be inside both foldl'
						-- x' y' should be possibly reduced to (Var "s#")
						-- or otherwise returned as atoms
					CState cnt'' (Right (foldl' foldExps (Add bodya bodyb) binds'))
				CState 0 (Left msg) -> (CState 0 (Left msg))
		CState 0 (Left msg) -> (CState 0 (Left msg))

-- let expressions, subexpressions can be atomic or complex
rcoExp (CState state (Right (Let sym expr body))) = 
	case rcoExp (CState state (Right expr)) of 
		CState cnt' (Right expr') -> 
			case rcoExp (CState cnt' (Right body)) of
				CState cnt'' (Right body') -> CState cnt'' (Right (Let sym expr' body'))
				err2 -> err2
		err -> err

-- pass errors up
rcoExp (CState _ (Left msg)) = CState 0 (Left msg)

foldExps body (sym, expr) = (Let sym expr body)

{- rcoExp (Let "s1" (Int 5) (Var "s1") )
	let assign = rcoExp (Int 5) in
	let body' = rcoExp (Var "s1") in
	(Let "s1" assign body')
-}

{- rcoExp (Add (Negate (Int 5) (Int 7)))
	let a = rcoAtm (Negate (Int 5)) in
	let b = rcoAtm (Int 7) in
	
	(Add (Var "s12") (Int 7)) 
-}

-- foldl' (\body (name, exp) -> (Let name exp body))

-- Define a type for our atomic expression operations. This type returns a pair, which
-- is either a new Var, if needed, or the old expression. The list is then a list of name
-- binding pairs, where the binding is an expression. The idea here is that when this function
-- returns to rcoExp, you'll create a bunch of let bindings from this list, and Exp is sub'd
-- wherever this was called from
type AtmResult = CompilerResult RCOState (Exp, [(String, Exp)])
rcoAtm :: AtmResult -> AtmResult
rcoAtm res@(CState _ (Right (Int _, _))) = res 
rcoAtm res@(CState _ (Right (Read, _))) = res
rcoAtm res@(CState _ (Right (Var _, _))) = res

-- negate expressions
rcoAtm (CState cnt (Right ((Negate expr), lst)) ) = 
	case rcoAtm (CState cnt (Right (expr, lst))) of
		(CState cnt' (Right (expr', lst'))) ->
			let newName = (makeSymName cnt') in 
			let binding = (newName, (Negate expr')) in
				(CState (cnt'+1) (Right ((Var newName), (binding:lst))) )
		err -> err
		

-- add expressions, here, both subexpressions must be atoms too
rcoAtm (CState cnt (Right (Add e1 e2, lst))) =
	case rcoAtm (CState cnt (Right (e1, lst))) of 
		(CState cnt' (Right (expr1, lst'))) ->
			case rcoAtm (CState cnt' (Right (e2, lst'))) of 
				(CState cnt'' (Right (expr2, lst''))) ->
					let newName = (makeSymName cnt'') in 
					let binding = (newName, (Add expr1 expr2)) in
						(CState (cnt''+1) (Right ((Var newName), (binding:lst''))) )
				err2 -> err2
		err -> err


-- let expressions, either of these expressions, expr or body,
-- can be atomic or complex
rcoAtm (CState cnt (Right (Let sym expr body, lst))) = 
	case rcoExp (CState cnt (Right expr)) of
		(CState cnt' (Right expr')) -> 
			case rcoExp (CState cnt' (Right body)) of
				(CState cnt'' (Right body')) -> 
					let newName = (makeSymName cnt'') 	 in
							-- made binding be the result of the let, bound to the new name
					let binding = (newName, (Let sym expr' body')) in
							-- Inserted (Var newName) as body
						(CState (cnt''+1) (Right ((Var newName), (binding:lst)))) 
				(CState _ (Left err2)) -> (CState 0 (Left err2))
		(CState _ (Left err)) -> (CState 0 (Left err))

		
-- Pass errors up
rcoAtm (CState _ (Left msg)) = CState 0 (Left msg)

-------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Pass: Partial Evaluation
type PartialResult = Exp

-- Primary recursive method
partialEvaluator :: PartialResult -> PartialResult
partialEvaluator (Int x) = (Int x)
partialEvaluator (Read) = (Read)
partialEvaluator (Negate (Int v)) = (Int (-v) )
partialEvaluator (Negate exp) = innerEvaluator (Negate (partialEvaluator exp))
partialEvaluator (Add (Int a) (Int b)) = (Int (a+b))
partialEvaluator (Add exp1 exp2) = innerEvaluator (Add (partialEvaluator exp1) (partialEvaluator exp2))
partialEvaluator (Var sym) = (Var sym)
partialEvaluator (Let sym exp1 exp2) = (Let sym (partialEvaluator exp1) (partialEvaluator exp2) )

-- Need this so that another attempt can be made to reduce inner expressions on the 'way back up'
innerEvaluator :: PartialResult -> PartialResult
innerEvaluator (Negate (Int v)) = (Int (-v))
innerEvaluator (Negate exp) = (Negate exp)
innerEvaluator (Add (Int a) (Int b)) = (Int (a+b))
innerEvaluator (Add exp1 exp2) = (Add exp1 exp2)




pg_u_blob@(CState _ (Right pg_u)) = uniquifyExp pg (UState Env.makeEnv 0)
pg_rco_blob@(CState _ (Right pg_rco)) = rcoExp (CState 0 (Right pg))
pg_pe = partialEvaluator pg

pg_pe_rco_blob@(CState _ (Right pg_pe_rco)) = rcoExp (CState 0 (Right pg_pe))

pg_rco_u_blob@(CState _ (Right pg_rco_u)) = uniquifyExp pg_rco (UState Env.makeEnv 0)

pg4_rco_blob@(CState _ (Right pg4_rco)) = rcoExp (CState 0 (Right pg4))