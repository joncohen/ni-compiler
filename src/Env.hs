module Env where
import Data.List as List

newtype Env a = Env [(String, a)] deriving (Eq, Show)

makeEnv :: Env a
makeEnv = Env [] 

extendEnv :: String -> a -> Env a -> Env a
extendEnv sym val (Env oldEnv) =
	Env $ ( sym, val ) : oldEnv 
	
lookup :: String -> Env a -> Either String a
lookup sym (Env env) =
	case List.lookup sym env of
		Just v -> Right v
		Nothing -> Left ("Symbol '" ++ sym ++ "' not found")
		
makeSymName :: (Show n, Num n) => n -> String
makeSymName n = "s" ++ show n