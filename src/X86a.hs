module X86a where

import Data.Int

data Reg = RSP
	| RBP | RAX | RBX  | RCX | RDX
	| RSI | RDI | R8 | R9 | R10
	| R11 | R12 | R13 | R14 | R15 deriving Eq
	
instance Show Reg where
	show RSP = "%rsp"
	show RBP = "%rbp"
	show RAX = "%rax"
	show RBX = "%rbx"
	show RCX = "%rcx"
	show RDX = "%rdx"
	show RSI = "%rsi"
	show RDI = "%rdi"
	show R8 = "%r8"
	show R9 = "%r9"
	show R10 = "%r10"
	show R11 = "%r11"
	show R12 = "%r12"
	show R13 = "%r13"
	show R14 = "%r14"
	show R15 = "%r15"

data SrcArg = SrcImm Int64 | SrcReg Reg | SrcMem Reg Int64
	deriving Eq
	
instance Show SrcArg where
	show (SrcImm v) = "$" ++ show v
	show (SrcReg reg) = show reg
	show (SrcMem reg offset) = show offset ++ "(" ++ show reg ++ ")"

data DstArg = DstReg Reg | DstMem Reg Int64
	deriving Eq

instance Show DstArg where
	show (DstReg reg) = show reg
	show (DstMem reg offset) = show offset ++ "(" ++ show reg ++ ")"

newtype Label = Label String deriving Eq

instance Show Label where
	show (Label name) = name ++ ":"
	
data Instr = 
	Addq SrcArg DstArg
	| Subq SrcArg DstArg
	| Movq SrcArg DstArg
	| Negq DstArg
	| Callq Label Integer
	| Retq
	| Pushq SrcArg
	| Popq SrcArg
	| Jmp Label deriving Eq
	
instance Show Instr where
	show (Addq src dst) =	"addq   " ++ show src ++ ", " ++ show dst
	show (Subq src dst) =	"subq   " ++ show src ++ ", " ++ show dst
	show (Movq src dst) =	"subq   " ++ show src ++ ", " ++ show dst
	show (Negq dst) =		"negq   " ++ show dst
	show (Callq lbl _) =	"callq  " ++ show lbl
	show (Retq) =			"retq"
	show (Pushq src) =		"pushq  " ++ show src
	show (Popq src) =		"popq  " ++ show src
	show (Jmp lbl) =		"jmp  " ++ show lbl

data Block a = Block { blockInfo :: a, instrs :: [Instr] } deriving (Eq)

instance (Show a) => Show (Block a) where
	show Block { blockInfo = bi, instrs = ins } =
		(show bi) ++ "\n\t" ++ foldl (\acc x -> acc ++ show x ++ "\n\t") "" ins
		
data X860 a = Program { progInfo :: a, cfg :: [(Label, Block a)] } deriving (Eq)

instance (Show a) => Show (X860 a) where
	show Program { progInfo = pi, cfg = cfgs } = 
		"\t" ++ (show pi) ++ "\n" ++ foldl (\acc x -> acc ++ show (snd x) ++ "\n") "" cfgs

x86a = Block { blockInfo = (Label "_main"), instrs=[
	(Movq (SrcImm 5) (DstReg RAX)),
	(Addq (SrcImm 37) (DstReg RAX)),
	(Retq)
]}

x86p = Program { progInfo = (Label ".global _main"), cfg=[
	(Label "_main", x86a)
]}

{--
        .global _main
_main: 
        movq    $5,  %rax
        addq    $37, %rax
        retq
--}