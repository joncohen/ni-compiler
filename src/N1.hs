module N1 where

import Env
import Text.Read
import Data.Int

data Exp = Int Int64
	| Read
	| Negate Exp
	| Add Exp Exp
	| Var String -- Reading variable value
	| Let String Exp Exp deriving (Show, Eq)
	
newtype N1 = Program Exp deriving (Show, Eq)

{-- 
	interpN1 interprets an N1 program
--}

interpN1 :: N1 -> IO (Either String Int64)
interpN1 (Program exp) = interpExp exp (makeEnv :: Env Int64)

interpExp :: Exp -> Env Int64 -> IO (Either String Int64)
interpExp (Int x) _ = return $ Right x
interpExp Read _ = do
	str <- getLine
	case (readEither str :: Either String Int64) of 
		Right v -> return $ Right v
		Left _ ->  return $ Left $ "Expected an Int64 when reading, received '" ++ str ++ "'"
interpExp (Negate exp) env = do
	v <- interpExp exp env
	return $ (0 -) <$> v
interpExp (Add exp1 exp2) env = do
	x <- interpExp exp1 env
	y <- interpExp exp2 env
	return $ (+) <$> x <*> y
interpExp (Var sym) env = do
	return $ Env.lookup sym env
interpExp (Let sym exp body) env = do
	exp' <- interpExp exp env
	case exp' of
		Right v -> interpExp body (Env.extendEnv sym v env)
		Left _ -> return exp'
		
		
n1 = Program (Add (Int 10) (Int 32))
n1' = Program (Add (Int 10) (Negate (Int 32)))
n1'' = Program (Let "x" (Int 32) (Add (Int 10) (Var "x")))


pg = (Let "x" 
	(Negate
		(Add (Int 10) (Int 30))
	)
	(Add (Var "x")
		(Add (Int 2) (Int 3))
	)
 ) -- Why can't I place this on the gutter ;.;


pg2 = (Add 
	(Add 
		(Add (Int 5) (Int 4) )
	(Int 3))
 (Int 2))


pg3 = (Add 
	(Add 
		(Add (Int 5) (Negate (Int 4)) )
	(Int 3))
 (Int 2))



pg4 = (Add (Int 5) 
	(Let "x" (Int 6) 
		(Let "y" (Int 6) 
			(Add (Var "x") (Var "y"))
		)
	)
 )



