module C0 where
import Data.Int
import Text.Read
import Env


{--
	atm ::= int | var
	exp ::= atm | read | -atm | atm + atm
	stmt ::= var = exp ;
	tail ::= return exp; | stmt tail
	
	C0 ::= (label: tail)+
--}

data Atom = Int Int64 | Var String deriving (Eq, Show)

data Exp = Atm Atom | Read | Sub Atom | Add Atom Atom deriving (Eq, Show)

data Stmt = Assign String Exp deriving (Eq, Show)

data Tail = Return Exp | Seq Stmt Tail deriving (Eq, Show)

data C0 a = Program { 
	progInfo :: a,
	cfg :: [(a, Tail)]
} deriving (Eq, Show)


c0 = (Seq (Assign "x" (Atm (Int 5)))
	(Seq (Assign "nx" (Sub(Var("x"))))
		( Return (Add (Var("nx")) (Int 120)) )
	))
	
run :: Tail -> IO (Either String Int64)
run it = interpTail it (makeEnv :: Env Int64)
	
	
--interpC0 :: C0 a -> IO (Either String Int64)
--interpC0 (Program progInfo (_, tail):stmts) = interpTail tail Env.makeEnv

interpTail :: Tail -> Env Int64 -> IO (Either String Int64)
interpTail (Return exp) env = do
	res <- interpExp exp env
	case (res) of
		Right v -> return $ Right v
		Left err -> return $ Left err
interpTail (Seq stmt tail) env = do
	res <- interpStmt stmt env 
	case (res) of
		Right (v, env') -> do
			res2 <- (interpTail tail env')
			case res2 of 
				Right v -> return $ Right v
				Left err -> return $ Left err
		Left err -> return $ Left err

interpStmt :: Stmt -> Env Int64 -> IO (Either String (Int64, Env Int64) )
interpStmt (Assign str exp) env = do
	res <- interpExp exp env 
	case (res) of
		Right v -> return $ Right (v, (extendEnv str v env))
		Left err -> return $ Left err
	
interpExp :: Exp -> Env Int64 -> IO (Either String Int64)
interpExp Read _ = do
	str <- getLine
	case (readEither str :: Either String Int64) of 
		Right v -> return $ Right v
		Left _ ->  return $ Left $ "Expected an Int64 when reading, received '" ++ str ++ "'"
		
interpExp (Atm atom) env = do
	res <- interpAtom atom env
	case (res) of
		Right v -> return $ Right v
		Left err -> return $ Left err
interpExp (Sub atom) env = do
	res <- interpAtom atom env
	case (res) of
		Right v -> return $ Right (-v)
		Left err -> return $ Left err
interpExp (Add a b) env = do
	resa <- interpAtom a env
	resb <- interpAtom b env
	case (resa) of 
		Right av -> case (resb) of 
			Right bv -> return $ Right (av + bv)
			Left err -> return $ Left err
		Left err -> return $ Left err


interpAtom :: Atom -> Env Int64 -> IO (Either String Int64)

interpAtom (Int v) env = do return $ Right v
interpAtom (Var sym) env = do
	case (Env.lookup sym env) of
		Right v -> return $ Right v
		Left err -> return $ Left err


