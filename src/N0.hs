module N0 where

import Data.Int

data Exp a = Int a 
	| Read 
	| Negate (Exp a) 
	| Add (Exp a) (Exp a)
	deriving (Show, Eq)

newtype N0 a = Program (Exp a) deriving (Show, Eq)

interpN0 :: Num a => (N0 a) -> a
interpN0 (Program exp) = interpExp0 exp

interpExp0 :: Num a => Exp a -> a
interpExp0 (Int x) = x
interpExp0 (Read) = 42
interpExp0 (Negate exp) = -(interpExp0 exp)
interpExp0 (Add exp1 exp2) = interpExp0 exp1 + interpExp0 exp2

instance Functor Exp where
	fmap f (Int x) = Int (f x)
	fmap f (Read) = Read
	fmap f (Negate exp) = Negate (fmap f exp)
	fmap f (Add exp1 exp2) = Add (fmap f exp1) (fmap f exp2)

n = Add (Int 32) (Int 10)
n' = Add (Int 7) (Add (Int 3) (Negate (Int 2)))
n'' = Negate (Add (Int 5) (Int 2))
n''' = Add (Negate (Int 5)) (Int 2)
n'''' = Add (Negate (Read)) (Int 2)

instance Functor N0 where
	fmap f (Program x) = Program (fmap f x)

isLeaf :: (Exp a) -> Bool
isLeaf (Int x) = True
isLeaf (Read) = True
isLeaf (Negate _) = False
isLeaf (Add _ _) = False

leafCount :: (N0 a) -> Integer
leafCount (Program exp) = leafCountH exp

leafCountH :: (Exp a) -> Integer
leafCountH (Int x) = 1
leafCountH (Read) = 1
leafCountH (Negate exp) = leafCountH exp
leafCountH (Add exp1 exp2) = leafCountH exp1 + leafCountH exp2

